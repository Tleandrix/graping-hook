package com.tleandrix.juegoAndroid;

import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public class Imagen extends Image {

	public Imagen(Texture textura, int ancho, int alto, int index) {
		super(textura);

		this.setWidth(ancho);
		this.setHeight(alto);

		this.setOrigin(ancho / 2, alto / 2);

	}

}