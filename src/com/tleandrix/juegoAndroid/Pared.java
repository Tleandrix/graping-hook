package com.tleandrix.juegoAndroid;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;

public class Pared {
	
	Imagen img;
	int dato;
	String tipo;
	
	public static final int TYPE_1 = 320;
	public static final int TYPE_2 = 160;
	public static final int TYPE_3 = 80;
	
	public Pared(String lado,String type, float y) {
		
		if (lado == "der") {
			
			if (type == "normal") {
				img = new Imagen(new Texture(Gdx.files.internal("data/rockder.png")),11,320,0);
				img.setPosition(240 - img.getWidth(), y);
				dato = 1;
				tipo = "normal";
			} else if (type == "pinche") {
				img = new Imagen(new Texture(Gdx.files.internal("data/derpinche.png")),20,320,0);
				img.setPosition(240 - img.getWidth(), y);
				dato = 1;
				tipo = "pinche";
			}
			
		} else if (lado == "izq") {
			
			if (type == "normal") {
				img = new Imagen(new Texture(Gdx.files.internal("data/rockizq.png")),11,320,0);
				img.setPosition(0, y);
				dato = 2;
				tipo = "normal";
			} else if (type == "pinche") {
				img = new Imagen(new Texture(Gdx.files.internal("data/izqpinche.png")),20,200,0);
				img.setPosition(0, y);
				dato = 2;
				tipo = "pinche";
			}
		}
		
		crearColision();
	}
	
	public void tick() {
		
		actColision();
		
	}
	
	Rectangle Coli;
	
	public void crearColision() { Coli = new Rectangle(img.getX(),img.getY(),img.getWidth(),img.getHeight());}
	
	public void actColision() { Coli.set(img.getX(),img.getY(),img.getWidth(),img.getHeight()); } 
	
	public Rectangle getColi() { return Coli; }
	
	public Imagen getImg() { return img; }
	
	public int getlado() { return dato; }
	
	public String getTipo() { return tipo; }
	
}
