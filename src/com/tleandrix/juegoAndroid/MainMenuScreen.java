package com.tleandrix.juegoAndroid;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public class MainMenuScreen implements Screen {
	
	private OrthographicCamera camera;
	private SpriteBatch batch;
	private Imagen background;

	private Rectangle point,start;
	
	GameMain game;
	private ShapeRenderer a;

	public MainMenuScreen(GameMain game) {
		
		this.game = game;
		Texture.setEnforcePotImages(false);

		camera = new OrthographicCamera(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		camera.setToOrtho(false);
		background = new Imagen(new Texture(Gdx.files.internal("data/menu.png")),Gdx.graphics.getWidth(),Gdx.graphics.getHeight(),0);
		background.setPosition(0, 0);
		
		batch = new SpriteBatch();
		a = new ShapeRenderer();

		start = new Rectangle(26,115,91,26);
		point = new Rectangle(Gdx.input.getX(),Gdx.input.getY(),10,10);
		
		
	}
	
	float gdxmX;
	float gdxmY;
	
	public void render(float delta) {
		camera.update();
		batch.begin();
		batch.setProjectionMatrix(camera.combined);
		background.draw(batch, 1);
		batch.end();
		
		gdxmX = Gdx.input.getX();
		gdxmY = Gdx.input.getY();
		
		
		if (Gdx.input.isTouched()) {
			Vector3 mouseps = new Vector3( gdxmX , gdxmY,0);

			camera.unproject(mouseps);
			point.set(mouseps.x,mouseps.y,10,10);
			if (Intersector.overlaps(point, start)) { game.setScreen(new World(game)); }

		}
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void show() {
		
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		
	}

}
