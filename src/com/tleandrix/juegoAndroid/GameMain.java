package com.tleandrix.juegoAndroid;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.Game;

public class GameMain extends Game {
	
	Stage stage;
	
	World world;


	public void create() {		

		setScreen(new MainMenuScreen(this));
		
	}

	public void dispose() {

	}

	@Override
	public void render() {		
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

        super.render();
        
	}

	public void resize(int width, int height) {

	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
}
