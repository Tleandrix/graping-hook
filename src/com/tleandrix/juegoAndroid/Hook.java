package com.tleandrix.juegoAndroid;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class Hook {

	Imagen hook,pj;
	OrthographicCamera worldScreen;
	
	ShapeRenderer line;
	
	public Hook(Imagen img, OrthographicCamera worldScreen) {
		
		hook = new Imagen(new Texture(Gdx.files.internal("data/hook.png")),7,9,0);
		hook.setPosition(img.getX(), img.getY());
		
		pj = img;
		
		this.worldScreen = worldScreen;
		
		line = new ShapeRenderer();
		
		crearColi();
		
	}
	
	private int angulo;
	private double radianangulo;
	float gdxmX;
	float gdxmY;
	
	int bx,by;
    float speed = 8.5f;
    
    boolean move = false;
    boolean grabed = false;
    
    int bdist;
	
    boolean touch,yatouch;
    
	public void tick() {
				
		gdxmX = Gdx.input.getX();
		gdxmY = Gdx.input.getY();
		
		boolean touch = Gdx.input.isTouched();
		
		if (Gdx.input.isTouched() && move == false && !yatouch) {
			Vector3 pjpos = new Vector3((float) pj.getX()+15,(float) pj.getY()+13.5f,0);
			Vector3 mouseps = new Vector3( gdxmX , gdxmY,0);

			worldScreen.unproject(mouseps);
	
			pjpos.sub(mouseps);
					
			Vector2 pos = new Vector2(pjpos.x,pjpos.y);
	
			angulo = (int) pos.angle();
			radianangulo = Math.toRadians(angulo);

			hook.setPosition(pjpos.x, pjpos.y);
			hook.setRotation(angulo);
			hook.setVisible(true);
			 
			move = true;
			grabed = false;
		}
		
		yatouch = touch;
		
		if (move == true && bdist < 35) {
			
			by -= Math.sin(radianangulo) * speed;	
			bx -= Math.cos(radianangulo) * speed;
			
			hook.setX(bx);
			hook.setY(by);
			
			bdist++;
			
			
			
		} else { hook.setPosition(pj.getX()+15, pj.getY()+13.5f); bx = (int) hook.getX(); by = (int) hook.getY(); bdist = 0; }
		
		line.begin(ShapeType.Line);
		line.setProjectionMatrix(worldScreen.combined);
		line.setColor(Color.BLACK);
		if (getMove() == true) { line.line(pj.getX()+15, pj.getY()+13.5f, hook.getX()+3.5f, hook.getY()+4.5f); hook.setVisible(true); } else { hook.setVisible(false); } 
		line.end();
		
		if (bdist == 0) { move = false; }
		
		actColi();
				
	}
	
	Interpolation a;
	
	public void grab(int lado) {
		if (move == true) {
			if (lado == 1)
			{ pj.addAction(Actions.moveTo(hook.getX()-24, hook.getY(), 0.2f, a.sine )); } else 
			{ pj.addAction(Actions.moveTo(hook.getX()+2, hook.getY(), 0.2f, a.sine));    }
			setMove(false);
			grabed = true;

		}
	}
	
	Rectangle COLI;
	
	public void crearColi() { COLI = new Rectangle (hook.getX(),hook.getY(),hook.getWidth(),hook.getHeight()); }
	
	public void actColi() { COLI.set(hook.getX(),hook.getY(),hook.getWidth(),hook.getHeight());	}
	
	public boolean getGrabed() { return grabed; }
	
	public Rectangle getColi() { return COLI; }
	
	public Imagen getHook() { return hook; }
	
	public void setMove(boolean dato) { move = dato; }
	
	public boolean getMove() { return move; }

}
