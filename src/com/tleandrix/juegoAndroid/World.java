package com.tleandrix.juegoAndroid;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.math.Intersector;

public class World implements Screen {
		
	GameMain game;
	OrthographicCamera worldScreen;
	Stage stage;
	
	ShapeRenderer a;
	Intersector coli;
	
	Array<Pared> paredes;
	
	int score = 0;
	
	
	SpriteBatch batch;
	BitmapFont scoretext;
	
	private static final int STATE_GAME_START = 1;
	private static final int STATE_GAME_PAUSE = 2;
	private static final int STATE_GAME_END = 3;
	
	
	int state;

	public World(GameMain game) {
		
		this.game = game;
		
		worldScreen = new OrthographicCamera();
		worldScreen.setToOrtho(false, Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		
		Texture.setEnforcePotImages(false);
		
		stage = new Stage();	
		
		paredes = new Array<Pared>();
		
		crearMundo();
		crearIdentidades();
		a = new ShapeRenderer();
		
		batch = new SpriteBatch();
		
		scoretext = new BitmapFont();
		scoretext.setColor(Color.WHITE);
		score -= 175;
		
	}
	

	Player player;
	
	public void crearIdentidades() {
		
		player = new Player(worldScreen);
		stage.addActor(player.getImg());
		
		stage.addActor(player.getHook().getHook());

	}
	
	Imagen background,piso,rockizq,rockder;
	
	public void crearMundo() {
		
		background = new Imagen(new Texture(Gdx.files.internal("data/background.png")),240,320,0);
		background.setPosition(0, 0);
		stage.addActor(background);
		
		piso = new Imagen(new Texture(Gdx.files.internal("data/piso.png")),240,75,0);
		piso.setPosition(0, 0);
		stage.addActor(piso);
		
		
		
		crearPared("der","normal",0);
		crearPared("izq","normal",0);
		crearPared("der","normal",320);
		crearPared("izq","normal",320);
		
		crearColisiones();
		
	}
	
	public void crearPared(String lado,String type, float y) {
		
		Pared a = new Pared(lado,type,y);
		stage.addActor(a.getImg());
		paredes.add(a);
		
	}
	
	int target,imgtarget,targetder,targetizq;
		
	public void tick() {
		
		batch.begin();
		batch.setProjectionMatrix(worldScreen.combined);
		scoretext.draw(batch, "score:"+ getScore(), 2, worldScreen.position.y+160);
		batch.end();
		actRectangle();
		
		worldScreen.position.set(worldScreen.position.x , player.getImg().getY()+100 , worldScreen.position.z);
		background.setPosition(0, worldScreen.position.y-160);

		player.tick();
		
		for (int i = 0; i < paredes.size; i++) { paredes.get(i).tick(); }
		
		
		if (score > (int) worldScreen.position.y-175) {
			
		} else { score = (int) worldScreen.position.y-175; }
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// LOGICA
		/*
		if (paredes.size != 0) {
			
			for (int f = 0; f < paredes.size; f++) {
				
				if (!Intersector.overlaps(WORLD_VIEW, paredes.get(f).getColi())) { imgtarget = f; }
				boolean imgout = !Intersector.overlaps(WORLD_VIEW, paredes.get(imgtarget).getColi());
				
				if (imgout) { paredes.get(imgtarget).img.setVisible(false); }
				
			}
		}
		*/
		
		for (int f = 0; f < paredes.size; f++) {
		
			if ( Intersector.overlaps(PARED_GEN, paredes.get(f).getColi()) && paredes.get(f).getlado() == 1 )  { targetder = f;}
		}
		
		for (int f = 0; f < paredes.size; f++) {
			
			if ( Intersector.overlaps(PARED_GEN, paredes.get(f).getColi()) && paredes.get(f).getlado() == 2)  { targetizq = f;}
		}
		
		boolean genparedder = Intersector.overlaps(PARED_GEN, paredes.get(targetder).getColi());
		boolean genparedizq = Intersector.overlaps(PARED_GEN, paredes.get(targetizq).getColi());

		if (genparedder) { } else { crearPared("der","normal",paredes.get(targetder).getColi().y+paredes.get(targetder).getImg().getHeight()); }
		if (genparedizq) { } else { crearPared("izq","pinche",paredes.get(targetizq).getColi().y+paredes.get(targetizq).getImg().getHeight()); }
		
		Vector3 mouseps = new Vector3( Gdx.input.getX() , Gdx.input.getY() , 0);

		worldScreen.unproject(mouseps);
		point.set(mouseps.x,mouseps.y,10,10);
		
		boolean pjClick = Intersector.overlaps(point, player.getColi()) && Gdx.input.isTouched();
		
		if (pjClick) { player.getHook().grabed = false; player.getHook().move = false;}
		
		System.out.println(targetder);
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// COLISION
						
		if ( player.getHook().getGrabed() == false && player.getHook().getMove() == true ) {
			
			for (int i = 0; i < paredes.size; i++) {
				if (player.getHook().getColi().overlaps(paredes.get(i).getColi()) && paredes.get(i).getlado() == 1 ) { player.getHook().grab(1); } else
				if (player.getHook().getColi().overlaps(paredes.get(i).getColi()) && paredes.get(i).getlado() == 2 ) { player.getHook().grab(2); }
			}
		}
		
		for (int i = 0; i < paredes.size; i++) {
			
			if (Intersector.overlaps(paredes.get(i).getColi(), player.getColi()))  { target = i; }

		}
		
		boolean colider = Intersector.overlaps(paredes.get(target).getColi(), player.getColi()) && paredes.get(target).getlado() == 1;
		boolean coliizq = Intersector.overlaps(paredes.get(target).getColi(), player.getColi()) && paredes.get(target).getlado() == 2;
		
		
		boolean colipiso = Intersector.overlaps(player.getColi(), GROUND_COLI);
		
		if (colipiso) { player.setGround(true); } else { player.setGround(false); }
		
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// MOVEMENT
	
		
		if(Gdx.input.isKeyPressed(Input.Keys.D) && !colider && player.getHook().grabed == false) { player.getImg().addAction(Actions.moveBy(1.5f, 0));  player.getHook().grabed = false;}
		if(Gdx.input.isKeyPressed(Input.Keys.A) && !coliizq && player.getHook().grabed == false) { player.getImg().addAction(Actions.moveBy(-1.5f, 0)); player.getHook().grabed = false;}
		if(Gdx.input.isKeyPressed(Input.Keys.W)) { player.getImg().addAction(Actions.moveBy(0, 12)); }
		
	    float accelX ;
	    
	    accelX = Gdx.input.getAccelerometerX();
		
		//if( accelX <= 1.5f 	 && player.getHook().grabed == false && !colider) { player.getImg().addAction(Actions.moveBy(1.5f, 0));  player.getHook().grabed = false;}
		//if( accelX >=  -1.5f && player.getHook().grabed == false && !coliizq) { player.getImg().addAction(Actions.moveBy(-1.5f, 0)); player.getHook().grabed = false;}
		
	    
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// EVENT
	    
	    if (colider && paredes.get(target).getTipo() == "pinche") { state = STATE_GAME_END; }
	    if (coliizq && paredes.get(target).getTipo() == "pinche") { state = STATE_GAME_END; }

		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// MISC
		
		a.begin(ShapeType.Line);
		a.setProjectionMatrix(worldScreen.combined);
		a.rect(player.getColi().x,player.getColi().y,player.getColi().width,player.getColi().height);
		a.rect(WORLD_VIEW.x,WORLD_VIEW.y,WORLD_VIEW.width,WORLD_VIEW.height);
		a.rect(PARED_GEN.x,PARED_GEN.y,PARED_GEN.width,PARED_GEN.height);
		a.end();
		
		
	}
	
	Rectangle WORLD_VIEW,GROUND_COLI,PARED_GEN;
	Rectangle point;
	
	public void crearColisiones() {
		GROUND_COLI = new Rectangle(piso.getX(),piso.getY(),piso.getWidth(),piso.getHeight());
		WORLD_VIEW = new Rectangle(0,0,240,320);
		PARED_GEN = new Rectangle(0,worldScreen.position.y+80,240,20); 
		point = new Rectangle();
	}
	
	public void actRectangle() {
		
		WORLD_VIEW.set(worldScreen.position.x-120, worldScreen.position.y-180 ,240 ,340);
		PARED_GEN.set(worldScreen.position.x-120,worldScreen.position.y+100,240,1); 

	}
	
	public int getScore() { return score; }
	
	public void render(float delta) {
		
        worldScreen.update();
        stage.act(Gdx.graphics.getDeltaTime());
        stage.setCamera(worldScreen);
        stage.draw();
        tick();
        updateGameEnd();
		
	}
	
	public void updateGameEnd() {
		if (state == STATE_GAME_END) { game.setScreen(new MainMenuScreen(game)); }
	}

	@Override
	public void resize(int width, int height) {
		stage.setViewport(width, height, true);		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		
		state = STATE_GAME_PAUSE;
		
	}

	@Override
	public void resume() {

		state = STATE_GAME_START;
		
	}

	@Override
	public void dispose() {
		stage.dispose();
		
	}

}
