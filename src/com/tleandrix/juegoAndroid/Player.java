package com.tleandrix.juegoAndroid;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class Player {
	
	Imagen img;
	Hook hook;
	
	int fallspeed = -2;
	
	public Player(OrthographicCamera worldScreen) {
		
		img = new Imagen(new Texture(Gdx.files.internal("data/pj.png")),30,27,0);
		img.setPosition(Gdx.graphics.getWidth()/2 - img.getWidth() / 2, 75);
		
		crearColision();
		
		hook = new Hook(img, worldScreen);

	}
	
	boolean onGround = false;
	
	float time = 0;
	
	public void tick() {
		
		hook.tick();

	    if (onGround == false && hook.getGrabed() == false) { 
	    	img.addAction(Actions.moveBy(0, fallspeed)); 
	    	
			  time += Gdx.graphics.getDeltaTime();
			  if(time >= 0.5) {																																					// Realizar accion cada 1 segundo!
				
				  	fallspeed -= 2;
				
			    time = 0;
		
			  }
	    	
	    } else { fallspeed = -2; }

		actColision();
		
	}
	
	Rectangle coli,Fcoli;
	
	public void crearColision() { coli = new Rectangle(img.getX(),img.getY(),img.getWidth(),img.getHeight()); Fcoli = new Rectangle(img.getX()-5,img.getY(),40,img.getHeight());}
	
	public void actColision() { coli.set(img.getX(),img.getY(),img.getWidth(),img.getHeight()); Fcoli.set(img.getX()-5,img.getY(),40,img.getHeight()); }
	
	public void setGround(boolean dato) { onGround = dato; }
	
	public Imagen getImg() { return img; }
	
	public Rectangle getColi() { return coli; }
	
	public Rectangle getFColi() { return Fcoli; }
	
	public Hook getHook() { return hook; }
	
}
